<?php
function jsonUser($obj){
    try {
        return array(
            'id'    => $obj->getId(),
            'cod'   => $obj->getUsercod(),
            'cel'   => $obj->getUserphone(),
            'tipo'  => $obj->getTypeuser()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonPedidos($obj){
    try {
        return array(
            'idped'     => $obj->getIdped(),
            'iduser'    => $obj->getIduser(),
            'user'      => $obj->getUser(),
            'fecha'     => $obj->getFecha(),
            'hora'      => $obj->getHora(),
            'idest'     => $obj->getIdestado(),
            'est'       => $obj->getEstado()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonPedDetalle($obj){
    try {
        return array(
            'iddet'     => $obj->getIddet(),
            'idped'     => $obj->getIdped(),
            'tipo'      => $obj->getTipoprod(),
            'prod'      => $obj->getProd(),
            'cantidad'  => $obj->getCantidad()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonOntMesh($obj){
    try {
        return array(
            'id'      => $obj->getId(),
            'tipo'    => $obj->getTipo(),
            'serie01' => $obj->getSerie01(),
            'serie02' => $obj->getSerie02(),
            'modelo'  => $obj->getModelo()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonCarONTMESH($datos){
    try {
        return array(
            'iddet'   => $datos[0],
            'id'      => $datos[1],
            'tipo'    => $datos[2],
            'serie01' => $datos[3],
            'serie02' => $datos[4],
            'modelo'  => $datos[5]
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonPedEntrega($obj){
    try {
        return array(
            'iddet'   => $obj->getIddet(),
            'tipo' => $obj->getTipoprod(),
            'cant' => $obj->getCantidad()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
function jsonProductos($obj){
    try {
        return array(
            'idprod' => $obj->getIdprod(),
            'prod'   => $obj->getProd(),
            'cant'   => $obj->getCant()
        );
    } catch (Exception $e) {
        return array('ocurrio un error en el json');
    }
}
?>