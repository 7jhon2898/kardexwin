<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/estilo.css">
    <link rel="stylesheet" href="../dist/fontawesome/css/all.min.css">
    <title>Home</title>
</head>

<body>


    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Kardex</a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="#">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="sidebar-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                <i class="fas fa-house-user"></i>
                                Pedido
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-download"></i>
                                Liquidacion
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <h2>Realizar pedido</h2><br>
                <div class="select_insu">
                    <div class="tit_insu">
                        <div class="row">
                        <h5 class="col-6">Insumo</h5>
                        <h5 class="col-4">Cantidad</h5>
                        </div>
                    </div>
                   <div class="acc_insu"> <div class="row">
                    <select class="custom-select col-6">
                        <option selected>Selecionar insumo</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                    <input type="text" class="form-control col-3" placeholder="Ingrese la cantidad que requiera">
                    <button class="btn btn-success col-2">Añadir Insumo Insumos</button>
                    </div></div>
                </div><br>
                <div class="table-responsive">
                    <table class="table table-striped table-sm  text-center">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Insumo</th>
                                <th>Cantidad</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </main>
        </div>

    </div>


</body>

</html>