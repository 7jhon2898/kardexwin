<?php
include_once 'functions.php';
include_once '../json/arrayJson.php';
$op = json_decode($_POST["op"]);
session_start();
switch($op){
    case 1://iniciar sesion
        try {
            $user = json_decode($_POST["user"]);
            $obj = new tblUser();
            if ($obj->validarCodigo($user[0]) == true){
                if($obj->validarPass($user) == true){
                    $data = jsonUser($obj->session_user($user[0]));
                    $_SESSION['user'] = [
                        'id'   => $data['id'],
                        'cod' => $data['cod'],
                        'cel'  => $data['cel'],
                        'tipo' => $data['tipo']
                    ];
                    if($data['tipo'] == 1){
                        echo json_encode('TU001');
                    }else if($data['tipo'] == 2){
                        echo json_encode('TU002');
                    }
                }else{
                    echo json_encode('ES002');
                }
            }else{
                echo json_encode('ES001');
            }
        } catch (Exception $e) {
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 2:
        try {
            $obj = new TablaPedidos();
            $data = $obj->showPed();
            $json = [];
            foreach ($data as $ped) {
                $json[] = jsonPedidos($ped);
            }
            echo json_encode($json);
        } catch (\Throwable $th) {
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 3:
        try {
            $idped = json_decode($_POST['idped']);
            $obj = new TablaPedDetalle();
            $data = $obj->pedDetalle($idped);
            $json =[];
            foreach($data as $ped){
                $json[] = jsonPedDetalle($ped);
            }
            echo json_encode($json);
        } catch (Exception $e) {
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 4:
        try {
            $obj = new TablaOntMesh();
            $data = $obj->MostrarMesh();
            $json = [];
            foreach($data as $mesh){
                $json[]=jsonOntMesh($mesh);
            }
            echo json_encode($json);
        } catch(Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 5:
        try {
            $obj = new TablaOntMesh();
            $data = $obj->MostrarOnt();
            $json = [];
            foreach($data as $ont){
                $json[]=jsonOntMesh($ont);
            }
            echo json_encode($json);
        } catch(Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 6://iniciar y agregar sesion
        try {
            $sessionaux = '';
            $datos = json_decode($_POST['datos']);
            $prod = jsonCarONTMESH($datos);
            $validar = false;
            if(!isset($_SESSION['ONTMESH'])){
                $_SESSION['ONTMESH'][$prod['id']]=$prod;
            }else{
                $sessionaux = $_SESSION['ONTMESH'];
                foreach($sessionaux as &$ontmesh){
                    if($ontmesh['id'] == $prod['id']){
                        $validar = true;
                        break;
                    }
                }
            }
            if($validar){
                $_SESSION['ONTMESH'] = $sessionaux;
            }elseif(!$validar){
                $_SESSION['ONTMESH'][$prod['id']] = $prod;
            }
            echo json_encode(true);
        } catch (Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 7://eliminar elemento de sesion
        try {
            $sessionaux = $_SESSION['ONTMESH'];
            $id = json_decode($_POST['id']);
            $cont = 0;
            foreach ($sessionaux as &$ontmesh) {
                if($ontmesh['id'] == $id){
                    array_splice($sessionaux,$cont,1);
                    break;
                }
                $cont++;
            }
            $_SESSION['ONTMESH'] = $sessionaux;
            echo json_encode(true);
        } catch (Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 8://enviar session ONTMESH
        try {
            echo json_encode($_SESSION['ONTMESH']);
        } catch (Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 9://eliminar valores de session ONTMESH
        try {
            if(isset($_SESSION['ONTMESH'])){
                unset($_SESSION['ONTMESH']);
            }
        } catch (Exception $e){
            echo json_encode(array("error"=>"error de registro"),JSON_FORCE_OBJECT);
        }
        break;
    case 10://destruir las sesiones
        session_destroy();
        echo json_encode(1);
        break;
    case 11://enviar datos de session de usuario
        echo json_encode($_SESSION['user']);
        break;
    case 12:
        try {
            if(!isset($_SESSION['ONTMESH'])){
                echo json_encode(false);
            }else{
                $sessionaux=$_SESSION['ONTMESH'];
                $tipo = json_decode($_POST['tipo']);
                $arreglo = [];
                foreach($sessionaux as $equipo){
                    if($equipo['tipo'] == $tipo){
                        $arreglo[] = $equipo;
                    }
                }
                echo json_encode($arreglo);
            }
        } catch (\Throwable $th) {
            echo json_encode(array("error"=>"error al mostrar ONT o"),JSON_FORCE_OBJECT);
        }
        break;
    case 13:
        try {
            if(!isset($_SESSION['ONTMESH'])){
                echo json_encode(false);
            }else{
                $sessionaux=$_SESSION['ONTMESH'];
                $validar = json_decode($_POST['validar']);
                $cont = 0;
                if($validar == 1){
                    $tipo = json_decode($_POST['tipo']);
                    foreach($sessionaux as $equipo){
                        if($equipo['tipo'] == $tipo){
                            $cont++;
                        }
                    }
                    echo json_encode($cont);
                }else if($validar == 2){
                    $contOnt = 0;
                    $contMesh = 0;
                    $arreglo = array();
                    foreach($sessionaux as $equipo){
                        if($equipo['tipo'] == 1){
                            $contOnt++;
                        }else if($equipo['tipo'] == 2){
                            $contMesh++;
                        }
                    }
                    $arreglo=['ont'=> $contOnt,'mesh'=> $contMesh];
                    echo json_encode($arreglo);
                }
            }
        } catch (\Throwable $th) {
            echo json_encode(array("error"=>"error al validar la cantidad"),JSON_FORCE_OBJECT);
        }
        breaK;
    case 14:
            $id = json_decode($_POST["id"]);
            $ped = new TablaPedidos();
            $det = new TablaPedDetalle();
            $prod = new TablaProducto();
            $ap = new AgregarPedidos();
            $data = $det->pedido($id);
            $equipo = [];
            $validarontmesh = false;
            $validarotros = false;
            foreach($data as $pedido){
                if($pedido->getTipoprod() != 1 && $pedido->getTipoprod() != 2){
                    $validarotros = true;
                }
                if($pedido->getTipoprod() == 1 || $pedido->getTipoprod() == 2){
                    $validarontmesh = true;
                }
                $equipo[] = jsonPedEntrega($pedido);
            }
            if($validarotros == true){
                $ap->agregarSinONTMESH($equipo);
            }
            if($validarontmesh == true){
                if(isset($_SESSION['ONTMESH'])){
                    $ap->agregarONTMESH($_SESSION['ONTMESH']);
                }
            }
            $prod->NuevaCantProd($equipo);
            $ped->updatePed($id);
            echo json_encode(true);
        break;
    case 15:{//mostrar inventario
        $obj = new TablaProducto();
        $data = $obj->mostrarProd();
        $json = [];
        foreach($data as $prod){
            $json [] = jsonProductos($prod);
        }
        echo json_encode($json);
        break;
    }
    case 16:{
        $id = json_decode($_POST['id']);
        $cant = json_decode($_POST['cant']);
        $obj = new TablaProducto();
        $obj->updateCantProd($cant,$id);
        echo json_encode(true);
        break;
    }
    case 17:{
        try {
            $data = json_decode($_POST['data']);
            $tipo = json_decode($_POST['tipo']);
            $obj = new TablaProducto();
            $ontmesh = new TablaOntMesh();
            if($tipo == 1 or $tipo ==2){
                $ontmesh->agregarONTMESH($data);
                $cant=$obj->mostrarCantidad($tipo);
                $obj->updateCantProd($cant+1,$tipo);
            }else if($tipo == 3){
                $obj->agregarOtros($data);
            }
            echo json_encode(true);
        } catch (\Throwable $th) {
            echo json_encode(array("error"=>"error al registrar elemento"),JSON_FORCE_OBJECT);
        }
        break;
    }
}
?>