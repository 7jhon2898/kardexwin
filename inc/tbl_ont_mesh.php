<?php
include_once 'functions.php';
class TablaOntMesh{
	public function connect(){
        $obj = new Conexion();
        return $obj -> getConn();
    }
    public function disconnect(){
        $obj = new Conexion();
        return $obj->disconnected();
    }
    public function MostrarMesh(){
    	$con  = $this->connect();
    	$ps = $con->prepare("SELECT * FROM tbl_ont_mesh WHERE tipo_prod_fk = 2 AND id_estado_fk = 3");
    	$ps->execute();
    	$array = array();
    	while($data = $ps->fetch(PDO::FETCH_ASSOC)){
    		$obj = new OntMesh();
    		$obj->setId($data['id']);
    		$obj->setTipo($data['tipo_prod_fk']);
    		$obj->setSerie01($data['serie01']);
    		$obj->setSerie02($data['serie02']);
    		$obj->setModelo($data['modelo']);
    		array_push($array,$obj);
    	}
		$this->disconnect();
		return $array;
    }
    public function MostrarOnt(){
    	$con  = $this->connect();
    	$ps = $con->prepare("SELECT * FROM tbl_ont_mesh WHERE tipo_prod_fk = 1 AND id_estado_fk = 3");
    	$ps->execute();
    	$array = array();
    	while($data = $ps->fetch(PDO::FETCH_ASSOC)){
    		$obj = new OntMesh();
    		$obj->setId($data['id']);
    		$obj->setTipo($data['tipo_prod_fk']);
    		$obj->setSerie01($data['serie01']);
    		$obj->setSerie02($data['serie02']);
    		$obj->setModelo($data['modelo']);
    		array_push($array,$obj);
    	}
		$this->disconnect();
		return $array;
    }
	public function agregarONTMESH($data){
		try {
			$con = $this->connect();
			$ps = $con->prepare("INSERT INTO tbl_ont_mesh (tipo_prod_fk,serie01,serie02,modelo,id_estado_fk)VALUES(?,?,?,?,?)");
			$ps->bindValue(1,$data[0],PDO::PARAM_INT);
			$ps->bindValue(2,$data[1]);
			$ps->bindValue(3,$data[2]);
			$ps->bindValue(4,$data[3]);
			$ps->bindValue(5,3,PDO::PARAM_INT);
			$ps->execute();
			$this->disconnect();
		} catch (Exception $e) {
			var_dump($e);
		}
	}
}
?>