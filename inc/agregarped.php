<?php
include_once 'functions.php';

class AgregarPedidos{
	public function connect(){
        $obj = new Conexion();
        return $obj -> getConn();
    }
    public function disconnect(){
        $obj = new Conexion();
        return $obj->disconnected();
    }
    public function agregarSinONTMESH($equipo){
        try {
            $fecha = date('Y-m-d');
            $con = $this->connect();
            foreach($equipo as $ped){
                if($ped['tipo'] != 1 && $ped['tipo'] != 2){
                    $ps = $con->prepare("INSERT INTO tbl_entrega_otros (id_det_fk,fecha_entrega,cant_liquido,fecha_liquido) VALUES (?,?,?,?)");
                    $ps->bindValue(1,$ped['iddet'],PDO::PARAM_INT);
                    $ps->bindValue(2,$fecha);
                    $ps->bindValue(3,0,PDO::PARAM_INT);
                    $ps->bindValue(4,'0-0-0');
                    $ps->execute();
                }
            } 
            $this->disconnect();
        } catch (Exception $e) {
            return $e;
        }
    }
    public function agregarONTMESH($ontmesh){
        try {
            $fecha = date('Y-m-d');
            $con = $this->connect();
            foreach($ontmesh as $ped){
                $ps = $con->prepare("INSERT INTO tbl_entrega_ontmesh (id_det_fk,id_ont_mesh_fk,fecha_entrega,id_estado_fk,fecha_liquido) VALUES (?,?,?,?,?)");
                $ps->bindValue(1,$ped['iddet'],PDO::PARAM_INT);
                $ps->bindValue(2,$ped['id'],PDO::PARAM_INT);
                $ps->bindValue(3,$fecha);
                $ps->bindValue(4,5,PDO::PARAM_INT);
                $ps->bindValue(5,'0-0-0');
                $ps->execute();
            }
            $this->disconnect();
        } catch  (Exception $e) {
            return $e;
        }
    }
}
?>